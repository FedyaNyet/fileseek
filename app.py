#!./venv/bin/python
from flask import Flask
app = Flask(__name__)

#Store file seek offsets for O(1) lookup
line_offset = [0]
with open('lines.txt', 'r') as file:
    for line in file:
        line_offset.append(line_offset[-1] + len(line))

#The only route defined in this app. 
#It accepts one required parameter of type int called line_index
@app.route('/lines/<int:line_index>')
def get_line_index(line_index):
    try:
        if(line_index-1<0):
            raise
        with open('lines.txt', 'r') as file:
            file.seek(line_offset[line_index-1])
            line = file.readline()
    except:
        line = 'line index out of bounds'
    finally:
        return line

#Run the flask built-in app server in debug when running `$ ./app.py`
if __name__ == '__main__':
    app.debug = True
    app.run()