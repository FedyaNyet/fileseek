#File Seek
## Pre-requisites
	To get running make sure you first have python and virtualenv installed one your system.

## Questionaire
1. How does your system work? (if not addressed in comments in source)
	It's a basic python app that uses a virtualenv python wrapper to contain the python libraries. The application code is all contained in app.py. When the application starts, the `lines.txt` file is opened and read into memory line by line, recording the file's offset at each new line. When the requests for a line number come in, the file needs to simply access the location of the stored line buffed to retreive the appropraite line.

2. How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?
	If should be fine. All it stores in memory is a list of indecies which will not exhaust memmory with the typical file. I acted under the assumption that becuase the file is stored on the server, the file won't be curated maliciously to overload the server. This MAY be done by creating a large file with only newlines which will store a list entry for every line.

3. How will your system perform with 100 users? 10000 users? 1000000 users?
	Generally this would require more insight into what sort of system the application will be hosted on. To by pass this I packaged the threading libarary `gevent` with this project to be able to scale up the number of processes servering the application. I didn't include this in the basic start.sh for simplicity sake, but to booste performance do `$ gunicorn app:app -k gevent -w 100` where the server will spawn 100 worker threads to handle the extra loads.

4. What documentation, websites, papers, etc did you consult in doing this assignment?
	Gunicorn settings: http://docs.gunicorn.org/en/stable/settings.html
	Read file optimization: http://stackoverflow.com/a/620492/385025

5. What third-party libraries or other tools does the system use? How did you choose each library or framework you used?
	* Flask - A basic framework with no frils and low overhead. I didn't need any fancy footwork to return file lines.
	* gunicorn - A basic python based wsgi web server. I chose the becuase it's easy to package with the code, but isn't the default debug server the comes with flask. In production this MAY be replaced with apache/mod_wsgi depending on project requirements.
	* gevent - A simple scheduler and thred manager that can be used with gunicorn to scale up performace if needed.

6. How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?
	I spent about 8 hours total. I wrote the initial app that returned the lines within the first 2 hour or so, but then spent the rest of my time writing docs, scripts and locating the best library for serving/scaling the app.

7. If you were to critique your code, what would you have to say about it?
	* I should be explicite with my exception handling.
